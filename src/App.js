import { useEffect, useState } from "react";
import Product from "./components/Product";
import Modal from "./components/Modal"
import  "./styles/App.css"
import stylesList from "./styles/List.module.scss"
import Header from "./components/Header";


function App() {

	const [products, setProducts] = useState([])
	const [firstModalOpen, setFirstModalOpen] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(false);

	
	const openFirstModal = (product) => {
		setFirstModalOpen(true);
		setSelectedProduct(product);
	};
	
	const closeFirstModal = () => {
		setFirstModalOpen(false);
		setSelectedProduct(false);
    };

	useEffect(() => {

		const url = "./products.json"
        fetch(url).then(resp => resp.json()).then(data => setProducts(data.products))

	}, [])


	const [cartItems, setCartItems] = useState([]);
    
    const addToCart = () => {
        setCartItems(prevCartItems => [...prevCartItems, selectedProduct]);
	};

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem("cart")) || [];
        setCartItems(cart);
    }, []);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cartItems));
	}, [cartItems]);
	

    const [cartsCount, setCartsCount] = useState()
	
	useEffect(() => {
        const carts = JSON.parse(localStorage.getItem("cart")|| []);
        setCartsCount(carts.length);
	}, [cartItems]);
	

    

	const [favorites, setFavorites] = useState([]);

	const addToFavorites = (product) => {
		const updatedProduct = { ...product, isFavorite: !product.isFavorite };

		setFavorites(prevFavorites => {
			const existingProductIndex = prevFavorites.findIndex(item => item.id === updatedProduct.id);

			if (existingProductIndex !== -1) {
				prevFavorites.splice(existingProductIndex, 1);
			} else {
				prevFavorites.push(updatedProduct);
			}

			return [...prevFavorites];
		});
	};

	useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        setFavorites(favorites);
    }, []);

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
	}, [favorites]);

		

	
	const [favoritesCount, setFavoritesCount] = useState();

	useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")|| []);
        setFavoritesCount(favorites.length);
	}, [favorites]);
	
	

	return (

		<div className="main">

			<Header
				cartsCount={cartsCount}
				favoritesCount={favoritesCount}
			/>

            <ul className={stylesList.list}>
				{products.map(product =>
					<Product
						title={product.title}
						price={product.price}
						number={product.number}
						id={product.id}
						key={product.id}
						image={product.image}
						openModal={() => openFirstModal(product)}
						addToFavorites={() => addToFavorites(product)}
					/>
				)}
			</ul>
			
			<Modal
                header="Додати цей товар у кошик?"
                isOpen={firstModalOpen}
                isClose={closeFirstModal}
                text={selectedProduct.title}
                actions={[
                    {
                        label: 'Ok',
						onClick: () => {
							addToCart()
							closeFirstModal()
						},						
						backgroundColor: 'green'
                    },
                    {
                        label: 'Cancel',
						onClick: () => closeFirstModal(),
						backgroundColor: 'gray'
                    }
                ]}
            />
        </div>
	)
}

export default App;
