// import { useState, useEffect } from 'react'
import styles from '../styles/Header.module.scss'

function Header(props) {


    return(
        <header className={styles.header}>
            <div className={styles.container}>
                <a href="#"><img src="./img/logo.svg" alt="logo" /></a>
                <div>
                    <img className={styles.icon} src="./img/basket-shopping-solid.svg" alt="busket" />
                    <span>{ props.cartsCount }</span>
                    <img className={styles.icon} src="./img/star-solid.svg" alt="favorite" />
                    <span>{ props.favoritesCount }</span>
                </div>
            </div>
            
        </header>
    )
}

export default Header